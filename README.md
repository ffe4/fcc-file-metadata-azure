# File Metadata Microservice

Azure Functions implementation of the File Metadata Microservice project from freeCodeCamp.org.

## Local Testing

For local testing make sure you set the UPLOAD_PAGE_URL in your local.settings.json. This is the url of the public/index.html file that will be displayed at the functionapp root.

## CLI Deployment

These instructions assume you have [Azure CLI](https://github.com/Azure/azure-cli) and [Azure Functions Core Tools](https://github.com/Azure/azure-functions-core-tools) installed.

```shell
# define variables
export AZURE_RESOURCE_GROUP_NAME=<string>
export AZURE_FUNCTION_APP_NAME=<string>
export AZURE_STORAGE_ACCOUNT_NAME=<string>
export AZURE_ENABLE_INSIGHTS=<bool>
# deploy template and save connection string
export AZURE_STORAGE_CONNECTION_STRING=$(az group deployment create \
--resource-group $AZURE_RESOURCE_GROUP_NAME \
--template-file azuredeploy.json \
--parameters \
  appName=$AZURE_FUNCTION_APP_NAME \
  storageName=$AZURE_STORAGE_ACCOUNT_NAME \
  enableInsights=$AZURE_ENABLE_INSIGHTS \
--query properties.outputs.connectionString.value)
# upload index.html
az storage blob upload -f public/index.html -c public -n index.html
# deploy function app
cd app
func azure functionapp publish $AZURE_FUNCTION_APP_NAME --build remote
```

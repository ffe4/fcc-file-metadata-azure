"""Azure Functions implementation of freeCodeCamp API Project: File Metadata Microservice"""
import logging
import json

import azure.functions as func

def main(req: func.HttpRequest) -> func.HttpResponse:
    """Return response with name, type and size of sent file."""
    logging.info('File Metadata Microservice processed a request.')

    file = req.files.get('upfile')
    if not file:
        return func.HttpResponse(
            json.dumps({
                "error": "must send file via field with name 'upfile'"
            }),
            status_code=400
        )
    return func.HttpResponse(json.dumps({
        "name": file.filename,
        "type": file.mimetype,
        "size": len(file.read()),
    }))
